awsAmplifyPubSub

This is a working sample of AWS Amplify using PubSub


Getting Started

1. Follow these instructions: https://aws-amplify.github.io/docs/js/pubsub
2. Create IoT policy
3. In AWS Console Congnito, click on 'Manage Identity Tools.' Create a new identity pool.  After creating copy the IdentityPoolId.
4. Through the AWS CLI paste the following command: aws iot attach-principal-policy --policy-name 'myIoTPolicy' --principal '<YOUR_COGNITO_IDENTITY_ID>'.
5. The principal is the IdentityPoolId.
6. Subscribe and create topic to send messages.
7. When running node process from command line add process.env.IOT_ENDPOINT and process.env.IDENTITY_POOL_ID.