// List of env variables
module.exports = {
	IDENTITY_POOL_ID: process.env.IDENTITY_POOL_ID,
	IOT_ENDPOINT: process.env.IOT_ENDPOINT,
	// for Cognito
	USER_POOL_ID: process.env.USER_POOL_ID,
	USER_POOL_WEB_CLIENT_ID: process.env.USER_POOL_WEB_CLIENT_ID,
}