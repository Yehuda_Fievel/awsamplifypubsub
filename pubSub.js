const AWS = require('aws-sdk');
const Amplify = require('aws-amplify');
const PubSub = Amplify.PubSub
const { AWSIoTProvider, MqttOverWSProvider } = require('@aws-amplify/pubsub/lib/Providers');
global.WebSocket = require('ws');


// Initialize the Amazon Cognito credentials provider
AWS.config.region = 'eu-central-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: process.env.IDENTITY_POOL_ID,
	IdentityId: 'IDENTITY_ID_RETURNED_FROM_YOUR_PROVIDER',
    Logins: {
		'cognito-identity.amazonaws.com': 'TOKEN_RETURNED_FROM_YOUR_PROVIDER'
   }
});

// Apply plugin with configuration
Amplify.default.addPluggable(new AWSIoTProvider({
	 aws_pubsub_region: 'eu-central-1',
	 aws_pubsub_endpoint: `wss://${process.env.IOT_ENDPOINT}-ats.iot.eu-central-1.amazonaws.com/mqtt`,
}));


Amplify.default.configure({
  Auth: {
    identityPoolId: process.env.IDENTITY_POOL_ID,
	userPoolId: process.env.USER_POOL_ID,
	userPoolWebClientId: process.env.USER_POOL_WEB_CLIENT_ID,
    region: 'eu-central-1',
	policyName: 'myIoTPolicy',
  }
});


Amplify.Auth.currentCredentials().then((info) => {
	console.log(info)
});


// Apply plugin with configuration
Amplify.default.addPluggable(new MqttOverWSProvider({
	aws_pubsub_endpoint: 'wss://iot.eclipse.org:8843/mqtt',
}));


const sub = PubSub.subscribe('myTopic').subscribe({
    next: data => console.log('Message received', data),
    error: error => console.error(error),
    close: () => console.log('Done'),
});


 setTimeout(async () => {
	 await PubSub.publish('myTopic', { msg: 'Hello to all' }).catch((err) => console.log(err));
}, 3000);

 setTimeout(async () => {
	 await PubSub.publish('myTopic', { msg: 'Hello again to all' }).catch((err) => console.log(err));
}, 6000);
